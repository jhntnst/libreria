﻿using AdministracionLibreria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdministracionLibreria.Repositories
{
    public interface ILibroRepository
    {
        Libro GetLibro();
    }
}
