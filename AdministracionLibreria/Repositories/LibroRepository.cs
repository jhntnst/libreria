﻿using AdministracionLibreria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdministracionLibreria.Repositories
{
    public class LibroRepository : ILibroRepository
    {
        public Libro GetLibro()
        {
            return new Libro
            {
                Titulo = "LA CONSPIRACION",
                AñoPublicacion = "2018",
                NumeroPaginas = 289,
                CantidadInventario = 4,
                IdAutor = 2,

                Autor = new Autor
                {
                    id = 2,
                    Nombre = "Dan Brown",
                    FechaNacimiento = "2-3-1987",
                    PaisOrigen = "E.U."
                }
            };
        }
    }
}
