﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdministracionLibreria.Models
{
    public class Autor
    {
        public int id { get; set; }
        public string Nombre { get; set; }
        public string FechaNacimiento { get; set; }
        public string PaisOrigen { get; set; }

    }
}
