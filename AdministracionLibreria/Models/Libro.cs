﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdministracionLibreria.Models
{
    public class Libro
    {

        public string Titulo { get; set; }
        public string AñoPublicacion { get; set; }
        public int NumeroPaginas { get; set; }
        public int CantidadInventario { get; set; }
        public int IdAutor { get; set; }

        public Autor Autor { get; set; }
        
    }
}
