﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdministracionLibreria.Models
{
    public class LibroViewModel
    {
        public string Titulo { get; set; }
        public string AñoPublicacion { get; set; }
        public int NumeroPaginas { get; set; }
        public int CantidadInventario { get; set; }
        public int IdAutor { get; set; }

        public int AutorId { get; set; }
        public string AutorNombre { get; set; }
        public string AutorFechaNacimiento { get; set; }
        public string AutorPaisOrigen { get; set; }


    }
}
