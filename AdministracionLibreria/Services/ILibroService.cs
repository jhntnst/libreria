﻿using AdministracionLibreria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdministracionLibreria.Services
{
    public interface ILibroService
    {
        LibroViewModel GetLibro();
    }
}
