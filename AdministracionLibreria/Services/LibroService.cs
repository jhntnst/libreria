﻿using AdministracionLibreria.Models;
using AdministracionLibreria.Repositories;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdministracionLibreria.Services
{
    public class LibroService : ILibroService
    {

        private readonly ILibroRepository _repository;
        private readonly IMapper _mapper;
        public LibroService(ILibroRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public LibroViewModel GetLibro()
        {
            var libro = _repository.GetLibro();

            return _mapper.Map<LibroViewModel>(libro);

            /*return new LibroViewModel()
            {
                Titulo = libro.Titulo,
                AñoPublicacion = libro.AñoPublicacion,
                NumeroPaginas = libro.NumeroPaginas,
                CantidadInventario = libro.CantidadInventario,
                IdAutor = libro.IdAutor
            };*/

        }
    }
}
