﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AdministracionLibreria.Models;
using AdministracionLibreria.DTOs;

namespace AdministracionLibreria.DTOs
{
    public class MapperConfig
    {
        public static MapperConfiguration MapperConfigration()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Libro, LibroDTO>(); //GET
                cfg.CreateMap<LibroDTO, Libro>(); //POST

                cfg.CreateMap<Autor, AutorDTO>(); //GET
                cfg.CreateMap<AutorDTO, Autor>(); //POST
            });
        }
    }
}
