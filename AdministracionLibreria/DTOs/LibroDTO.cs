﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AdministracionLibreria.DTOs
{
    public class LibroDTO
    {
        [Required(ErrorMessage ="El titulo es requerido")]
        [StringLength(50)]
        public string Titulo { get; set; }
        [Required(ErrorMessage = "El Año de publicacion es requerido")]
        public string AñoPublicacion { get; set; }
        public int NumeroPaginas { get; set; }
        public int CantidadInventario { get; set; }
        public int IdAutor { get; set; }
    }
}
