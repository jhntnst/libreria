﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AdministracionLibreria.DTOs
{
    public class AutorDTO
    {
        public int id { get; set; }
        [Required(ErrorMessage ="Es requerito el nombre")]
        [StringLength(40)]
        public string Nombre { get; set; }
        public string FechaNacimiento { get; set; }
        [StringLength(40)]
        public string PaisOrigen { get; set; }

    }
}
