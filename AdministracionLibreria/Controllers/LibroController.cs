﻿using AdministracionLibreria.DTOs;
using AdministracionLibreria.Models;
using AdministracionLibreria.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdministracionLibreria.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class LibroController : Controller
    {

        private readonly IMapper _mapper;

        public LibroController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult Add(LibroDTO request)
        {
            Libro libro = _mapper.Map<Libro>(request);
            return Ok();
        }


        //--------------------
        [HttpGet("{id}")]
        public string Get(int id)
        {
            //codigo para leer con el metodo get
            return id switch
            {
                1 => "libro1",
                2 => "Autor1",
                _ => throw new NotSupportedException("el id no es valido")
            };
        }

        [HttpPost]
        public string Post(LectruraEscrituraDTO lecturaEscritura)
        {
            //Guarda perfil en la base de datos
            return lecturaEscritura.Nombre;

        }

        public class LectruraEscrituraDTO
        {
            public string Nombre { get; set; }
            public string Apellido { get; set; }
        }

        /*
        private readonly ILibroService _service;

        
        public LibroController(ILibroService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            try
            {
                var libro = _service.GetLibro();
                return View(libro);
            }
            catch (Exception ex)
            {
                return Content("Ocurrio un errorrrrr" + ex.Message);
            }
         */   
        }
    }
