﻿using AdministracionLibreria.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdministracionLibreria.Profiels
{
    public class LibroProfile : Profile
    {
        public LibroProfile()
        {
            CreateMap<Libro, LibroViewModel>();//GET
            CreateMap<LibroViewModel, Libro>();//POST
        }
    }
}
